package producer;

import java.lang.reflect.InvocationTargetException;

public class ProducerImpl<T> implements Producer<T> {

    private String instanceClassName;

    public ProducerImpl(String instanceClassName) {

        this.instanceClassName = instanceClassName;
    }


    @Override
    public T create() {

        try {

            T instance = (T) Class.forName(instanceClassName).getConstructor().newInstance();

            return instance;

        } catch (InstantiationException
                | IllegalAccessException
                | InvocationTargetException
                | NoSuchMethodException
                | ClassNotFoundException e) {

            e.printStackTrace();
        }

        return null;
    }
}
