package producer;

public interface Producer<T> {

    T create();
}
