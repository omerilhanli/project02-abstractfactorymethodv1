package maker;

import maker.decision.DecideOn;
import producer.Producer;

import java.lang.reflect.InvocationTargetException;

public class ProducerMaker {

    public static Producer createProducer(DecideOn decideOn) {

        try {

            Producer producer = (Producer) Class

                    .forName(decideOn.getProducerName())

                    .getConstructor(String.class).newInstance(decideOn.getInstanceName());

            return producer;

        } catch (InstantiationException
                | IllegalAccessException
                | InvocationTargetException
                | NoSuchMethodException
                | ClassNotFoundException e) {

            e.printStackTrace();
        }


        return null;
    }

}
