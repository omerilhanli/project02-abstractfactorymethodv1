package maker.decision;

import data.CustomerImpl;
import data.ProductImpl;
import data.UserImpl;
import producer.ProducerImpl;

public enum DecideOn {

    /*
            - Bu noktada sisteme eklenen her yeni Obje Class bilgisi buraya değişken olarak eklenir ve
              Client (Program) tarafından çağırıp kullanmak yeterli
     */

    PRODUCT_01(ProducerImpl.class.getName(), ProductImpl.class.getName()),

    USER_01(ProducerImpl.class.getName(), UserImpl.class.getName()),

    CUSTOMER_01(ProducerImpl.class.getName(), CustomerImpl.class.getName());


    private String producerName;

    private String instanceName;

    DecideOn(String producerName, String instanceName) {

        this.producerName = producerName;

        this.instanceName = instanceName;
    }

    public String getProducerName() {

        return producerName;
    }

    public String getInstanceName() {

        return instanceName;
    }
}
