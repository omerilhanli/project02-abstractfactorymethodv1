package APPLICATION;

import data.contract.BaseObject;
import maker.ProducerMaker;
import maker.decision.DecideOn;
import producer.Producer;

public class Program {

    public static void main(String[] args) {

        Producer<BaseObject> producer = ProducerMaker.createProducer(DecideOn.CUSTOMER_01);

        BaseObject object = producer.create();

        System.out.println("::toString() -> " + object.toString());


    }
}
